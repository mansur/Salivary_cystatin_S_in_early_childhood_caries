import pandas as pd
import numpy as np
from sklearn.preprocessing import RobustScaler
from keras.utils.vis_utils import plot_model
import random
import keras
from keras import models
from keras import layers
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import label_binarize
from sklearn.multiclass import OneVsRestClassifier
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve, auc
from keras import Input
from keras.models import Model
import matplotlib.pyplot as plt
import sklearn.metrics as metrics
from sklearn.metrics import accuracy_score
from sklearn.metrics import classification_report
import numpy as np
import matplotlib.pyplot as plt
from keras import regularizers
from itertools import cycle
from sklearn import svm, datasets
from sklearn.metrics import roc_curve, auc
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import label_binarize
from sklearn.multiclass import OneVsRestClassifier
from sklearn.metrics import roc_auc_score
from scipy.interpolate import interp1d


def build_model():
    model = models.Sequential()
    model.add(layers.Dense(32, kernel_regularizer=regularizers.l2(0.01), activation='relu', input_shape=(22,)))
    model.add(layers.Dense(32, kernel_regularizer=regularizers.l2(0.01), activation='relu'))
    model.add(layers.Dense(1, activation='sigmoid'))
    model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
    return model


def create_dataset(gf, gt):

  Xg, ys = [], []
  lst = gf.shape[0] + gt.shape[0]
  f_counter = 0
  t_counter = 0

  for i in range(0, lst):
    rnd = random.randint(0, 100)
    if (rnd >= 50 and f_counter < gf.shape[0]) or (rnd < 50 and t_counter >= gt.shape[0]):
        z = gf.iloc[f_counter, :]
        Xg.append(z)
        ys.append(1)
        f_counter += 1

    elif(rnd < 50 and t_counter < gt.shape[0]) or (rnd >= 50 and f_counter >= gf.shape[0]):
        z = gt.iloc[t_counter, :]
        Xg.append(z)
        ys.append(0)
        t_counter += 1

  return np.array(Xg), np.array(ys)

case_0_df=pd.read_csv('C:\data\case-1.csv')

case_1_df=pd.read_csv('C:\data\case-0.csv')

g_scale_column = ['Cystatins levels', 'Age', 'Gender', 'Weight', 'Order', 'Father edu', 'Mother edu'
, 'Father age', 'Mother age', 'brush', 'floss', 'mouth wash',  'q3','q4'
, 'q5', 'q6', 'q8','q9','q10','q11','q12','q13']

g_scaler = RobustScaler()
g_scaler = g_scaler.fit(case_0_df[g_scale_column])
case_0_df.loc[:, g_scale_column] = g_scaler.transform(case_0_df[g_scale_column].to_numpy())
g_scaler=g_scaler.fit(case_1_df[g_scale_column])
case_1_df.loc[:, g_scale_column] = g_scaler.transform(case_1_df[g_scale_column].to_numpy())

Xg, y = create_dataset(case_0_df, case_1_df)

train_data = Xg[0:32, :]
#train_data = Xg[0:32, 1:]

train_lable = y[0:32]

k = 5

num_val_samples = len(train_data) // k
num_epochs = 100
all_scores = []
for i in range(k):
    print('processing fold #', i)
    val_data = train_data[i * num_val_samples: (i + 1) * num_val_samples]
    val_targets = train_lable[i * num_val_samples: (i + 1) * num_val_samples]
    partial_train_data = np.concatenate(
        [train_data[:i * num_val_samples],
         train_data[(i + 1) * num_val_samples:]],
        axis=0)
    partial_train_targets = np.concatenate(
        [train_lable[:i * num_val_samples],
         train_lable[(i + 1) * num_val_samples:]],
        axis=0)
    model = build_model()
    history = model.fit(partial_train_data, partial_train_targets,
                        epochs=num_epochs, batch_size=20, validation_data=(val_data, val_targets), verbose=0)
    history_dict = history.history
    history_dict = history.history
    acc = history.history['accuracy']
    loss_values = history_dict['loss']
    val_loss_values = history_dict['val_loss']
    epochs = range(1, len(acc) + 1)
    acc_values = history_dict['accuracy']
    val_acc_values = history_dict['val_accuracy']
    epochs = range(1, len(acc) + 1)


test_data = Xg[32:, :]
#test_data = Xg[32:, 1:]

test_lable = y[32:]

print(test_lable)
score = model.evaluate(test_data, test_lable, verbose=1)
print("Test Accuracy:", score[1])

preds = model.predict(test_data)
y_pred = np.where(preds < 0.5, 0, 1)

#tn, fp, fn, tp = metrics.confusion_matrix(test_lable, y_pred).ravel()

auc = roc_auc_score(test_lable, preds)
print('AUC: %.2f' % auc)
fpr1, tpr1, thresholds1 = roc_curve(test_lable, preds)
optimal_idx1 = np.argmax(tpr1 - fpr1)
optimal_idx2 = np.argmin(np.sqrt((np.power((1-tpr1), 2))+ (np.power((1-(1-fpr1)), 2))))
print("Roc", thresholds1[optimal_idx1])
print("Roc", thresholds1[optimal_idx2])
plt.plot(fpr1, tpr1, label='ROC curve (area = {0:0.2f})'
                                            ''.format(auc), color='deeppink')
plt.plot([0, 1], [0, 1], color='darkblue', linestyle='--')
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Receiver Operating Characteristic (ROC) Curve')
plt.legend()
plt.show()
tn, fp, fn, tp = metrics.confusion_matrix(test_lable, y_pred).ravel()
confusion_matrix = metrics.confusion_matrix(test_lable, y_pred)
print(confusion_matrix)
Specificity = tn / (tn + fp)
Sensitivity = tp / (fn + tp)
print(Specificity)
print(Sensitivity)
