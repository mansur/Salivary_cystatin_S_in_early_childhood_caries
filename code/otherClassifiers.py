import pandas as pd
import numpy as np
from sklearn.preprocessing import RobustScaler
import random
from sklearn.ensemble import RandomForestClassifier
from sklearn import metrics
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
from sklearn.metrics import roc_auc_score
from sklearn.metrics import roc_curve, auc
import seaborn as sns
from sklearn import tree
from sklearn.model_selection import cross_val_score
from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn import svm
from xgboost import XGBClassifier



def create_dataset(fx, tx):
  Xs, ys = [], []
  lst = fx.shape[0] + tx.shape[0]
  print(lst)
  f_index = 0
  t_index = 0
  for i in range(0, lst):
    rnd = random.randint(0,100)
    if (rnd >= 50 and f_index < fx.shape[0]) or (rnd < 50 and t_index >= tx.shape[0]):
        z=fx.iloc[f_index, :]
        Xs.append(z)
        ys.append(1)
        f_index += 1
    elif(rnd < 50 and t_index < tx.shape[0]) or (rnd >= 50 and f_index >= fx.shape[0]):
        z = tx.iloc[t_index, :]
        Xs.append(z)
        ys.append(0)
        t_index += 1

  return np.array(Xs), np.array(ys)

case_0_df = pd.read_csv("C:\data\case-1.csv")
case_1_df = pd.read_csv("C:\data\case-0.csv")

Xt, y = create_dataset(case_0_df, case_1_df)

train_data = Xt[0:32, :]
#train_data = Xt[0:32, 1:]

train_lable = y[0:12]


x_test = Xt[12:, 1:]
y_test = y[12:]


model = XGBClassifier()
model.fit(train_data, train_lable)
y_pred = model.predict(x_test)
print("XGboost---> Accuracy:", metrics.accuracy_score(y_test, y_pred))
# print("XG--> Recall:", metrics.recall_score(y_test, y_pred))
# print("XG--> Precision:", metrics.precision_score(y_test, y_pred))
# print("XG--> f1:", metrics.f1_score(y_test, y_pred))
tn, fp, fn, tp = metrics.confusion_matrix(y_test, y_pred).ravel()
Specificity = tn / (tn + fp)
Sensitivity = tp / (fn + tp)
print(Specificity)
print(Sensitivity)
auc = roc_auc_score(y_test, y_pred)
print('AUC: %.2f' % auc)
fpr1, tpr1, thresholds1 = roc_curve(y_test, y_pred)
optimal_idx1 = np.argmax(tpr1 - fpr1)
optimal_idx2 = np.argmin(np.sqrt((np.power((1-tpr1),2))+ (np.power((1-(1-fpr1)), 2))))
print("Roc", thresholds1[optimal_idx1])
print("Roc", thresholds1[optimal_idx2])
plt.plot(fpr1, tpr1, label='ROC curve (area = {0:0.2f})'
                                            ''.format(auc), color='deeppink')
plt.plot([0, 1], [0, 1], color='darkblue', linestyle='--')
plt.xlabel('False Positive Rate')
plt.ylabel('True Positive Rate')
plt.title('Receiver Operating Characteristic (ROC) Curve')
plt.legend()
plt.show()

clf = RandomForestClassifier(n_estimators=100)
clf.fit(train_data, train_lable)
y_pred = clf.predict(x_test)
print("RandomForest---> Accuracy:", metrics.accuracy_score(y_test, y_pred))
# print("RF--> Recall:",metrics.recall_score(y_test, y_pred))
# print("RF--> Precision:",metrics.precision_score(y_test, y_pred))
# print("RF--> f1:",metrics.f1_score(y_test, y_pred))
tn, fp, fn, tp = metrics.confusion_matrix(y_test, y_pred).ravel()
Specificity = tn / (tn + fp)
Sensitivity = tp / (fn + tp)
print(Specificity)
print(Sensitivity)
importances = clf.feature_importances_
std = np.std([tree.feature_importances_ for tree in clf.estimators_],
             axis=0)
indices = np.argsort(importances)[::-1]

#Print the feature ranking
print("Feature ranking:")

for f in range(train_data.shape[1]):
    print("%d. feature %d (%f)" % (f + 1, indices[f], importances[indices[f]]))

#Plot the impurity-based feature importances of the forest
plt.figure()
plt.title("Feature importances analysis")
a=["(A)","(N)","(G)","(F)","(O)","(P)","(B)","(Q)","(I)","(H)","(D)","(E)","(C)",
   "(K)","(R)","(L)","(S)","(M)","(J)","(G)","(V)","(U)","(T)"]
plt.bar(range(train_data.shape[1]), importances[indices],
        color="blue", yerr=std[indices], align="center")
plt.xticks(range(train_data.shape[1]), a, fontsize=10)
plt.xlim([-1, train_data.shape[1]])
plt.savefig("pic_pol.png")
plt.show()

SVM = svm.SVC(kernel='linear')
SVM.fit(train_data, train_lable)
y_pred = SVM.predict(x_test)
print("SVM---> Accuracy:",metrics.accuracy_score(y_test, y_pred))
# print("SVM--> Recall:",metrics.recall_score(y_test, y_pred))
# print("SVM--> Precision:",metrics.precision_score(y_test, y_pred))
# print("SVM--> f1:",metrics.f1_score(y_test, y_pred))
tn, fp, fn, tp = metrics.confusion_matrix(y_test, y_pred).ravel()
Specificity = tn / (tn + fp)
Sensitivity = tp / (fn + tp)
print(Specificity)
print(Sensitivity)

logreg = LogisticRegression()
logreg.fit(train_data,train_lable)
y_pred=logreg.predict(x_test)
print("Logistic--> Accuracy:", metrics.accuracy_score(y_test, y_pred))
# print("LR--> Recall:", metrics.recall_score(y_test, y_pred))
# print("LR--> Precision:", metrics.precision_score(y_test, y_pred))
# print("LR--> f1:", metrics.f1_score(y_test, y_pred))
tn, fp, fn, tp = metrics.confusion_matrix(y_test, y_pred).ravel()
Specificity = tn / (tn + fp)
Sensitivity = tp / (fn + tp)
print(Specificity)
print(Sensitivity)

